Generate a SiLA Server/Client package
=====================================

Use the command :doc:`sila2-codegen new-package </content/code_generator/new_package>` to generate a SiLA Server/Client Python package.

Arguments (excerpt):

- ``--package-name``: Name of the generated Python package (required, must be a valid Python identifier)
- ``--output-dir``: Output directory (default: current directory)
- ``feature_definitions``: Feature definition files of the features you want to implement (``.sila.xml`` files, optional)

Example
-------

Let's build a SiLA Server which implements the `GreetingProvider <https://gitlab.com/SiLA2/sila_base/-/blob/master/feature_definitions/org/silastandard/examples/GreetingProvider-v1_0.sila.xml>`_ example feature.

1. Create the package
^^^^^^^^^^^^^^^^^^^^^

1. Create a new directory and open it in a terminal
2. If not done already: Install the ``sila2`` library by running ``pip install sila2`` (required Python >= 3.8)
3. Download the feature definition file into this directory: `GreetingProvider-v1_0.sila.xml <https://gitlab.com/SiLA2/sila_base/-/blob/master/feature_definitions/org/silastandard/examples/GreetingProvider-v1_0.sila.xml>`_
4. Generate the package by running ``sila2-codegen new-package --package-name my_sila2_package GreetingProvider-v1_0.sila.xml``

Your current directory should now contain the following files and sub-directories:

.. code-block::

    ├── GreetingProvider-v1_0.sila.xml
    ├── my_sila2_package
    │   ├── feature_implementations
    │   │   ├── greetingprovider_impl.py
    │   │   └── ...
    │   ├── generated
    │   │   ├── greetingprovider
    │   │   │   └── ...
    │   │   └── ...
    │   ├── server.py
    │   └── ...
    └── pyproject.toml

You can safely remove the file ``GreetingProvider-v1_0.sila.xml``, it was copied to the generated package files.

- The file ``pyproject.toml`` is required for installing the generated package
- The package code itself is located in the ``my_sila2_package`` directory
- The file ``server.py`` defined the SiLA Server
- The directory ``generated/greetingprovider`` contains multiple files required for implementing the ``GreetingProvider`` feature
- The file ``feature_implementations/greetingprovider_impl.py`` is where you implement the feature

2. Set server information
^^^^^^^^^^^^^^^^^^^^^^^^^

Open the file ``server.py``. It contains a class ``Server``:

.. code-block:: python

    class Server(SilaServer):
        def __init__(self, server_uuid: Optional[UUID] = None):
            # TODO: fill in your server information
            super().__init__(
                server_name="TODO",
                server_type="TODO",
                server_version="0.1",
                server_description="TODO",
                server_vendor_url="https://gitlab.com/SiLA2/sila_python",
                server_uuid=server_uuid,
            )

Replace the server name, type, version, description and vendor URL with appropriate values. This information will be available to all connected clients.

The default values (``"TODO"``, ...) are valid values for these fields, so if you are just experimenting with the SDK, you don't need to do this.

3. Implement the GreetingProvider feature
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The `GreetingProvider <https://gitlab.com/SiLA2/sila_base/-/blob/master/feature_definitions/org/silastandard/examples/GreetingProvider-v1_0.sila.xml>`_ feature defines the command ``SayHello`` and the property ``StartYear``:

- ``SayHello`` expects a ``String`` parameter "Name" and returns the ``String`` "Greeting"
- ``StartYear`` returns the year when the SiLA Server has been started

To implement the feature, open the file ``my_sila2_package/feature_implementations/greetingprovider_impl.py``.

It contains a few imports and the following class:

.. code-block:: python

    class GreetingProviderImpl(GreetingProviderBase):
        def get_StartYear(self, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> int:
            raise NotImplementedError  # TODO

        def SayHello(self, Name: str, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> SayHello_Responses:
            raise NotImplementedError  # TODO

We can ignore the ``metadata`` arguments for now. See :doc:`feature_implementation` for instructions on how to work with SiLA Client Metadata.

To implement the ``StartYear`` property, the method ``get_StartYear`` must return an :py:class:`int`.
You can simply replace ``raise NotImplementedError  # TODO`` with ``return 2022``.

The command ``SayHello`` is specified to return a ``SayHello_Responses`` object.
That class is imported from the file ``my_sila2_package/generated/greetingprovider/greetingprovider_types``, where it is defined like this:

.. code-block:: python

    class SayHello_Responses(NamedTuple):
        Greeting: str

The parameter ``Name`` is given as a :py:class:`str` object.
Replace ``raise NotImplementedError  # TODO`` with ``return SayHello_Responses(Greeting="Hello " + Name)``.

Congratulations, you have now implemented a the GreetingProvider feature and can :doc:`start the server <start_server>`.
