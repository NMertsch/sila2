Structure of generated packages
===============================

The following example assumes that the ``GreetingProvider`` example feature was downloaded from `here <https://gitlab.com/SiLA2/sila_base/-/blob/master/feature_definitions/org/silastandard/examples/GreetingProvider-v1_0.sila.xml>`_ into the current directory.
To generate a SiLA 2 Server/Client package, run the command :doc:`sila-codegen new-package <new_package>`:

.. code-block:: shell

    $ sila2-codegen new-package --package-name my_sila2_package --output-directory my-sila2-package GreetingProvider-v1_0.sila.xml

This will create a new directory ``my-sila2-package`` with the following content:

.. parsed-literal::

    my-sila2-package/
    ├── my_sila2_package
    │   ├── feature_implementations
    │   │   ├── greetingprovider_impl.py
    │   │   └── __init__.py
    │   ├── generated
    │   │   ├── client.py
    │   │   ├── greetingprovider
    │   │   │   ├── greetingprovider_base.py
    │   │   │   ├── greetingprovider_client.py
    │   │   │   ├── greetingprovider_errors.py
    │   │   │   ├── greetingprovider_feature.py
    │   │   │   ├── GreetingProvider.proto
    │   │   │   ├── GreetingProvider.sila.xml
    │   │   │   ├── greetingprovider_types.py
    │   │   │   └── __init__.py
    │   │   └── __init__.py
    │   ├── __init__.py
    │   ├── __main__.py
    │   ├── py.typed
    │   └── server.py
    ├── pyproject.toml
    └── setup.py

The important pieces are:

- ``server.py``: Define the SiLA Server behavior here
- ``feature_implementations/``: Implement the SiLA Server features here (in the ``..._impl.py`` files)
- ``setup.cfg``: Add your package metadata and requirements here (important if you plan to distribute the package)
- ``generated/``: Not intended to be modified (see :doc:`sila2-codegen update <update>`), contains the SiLA Client class and a few classes required for feature implementations

See :doc:`/content/server/index` on how to implement and run the SiLA Server.

See :doc:`/content/client/index` on how to use the SiLA Client.

See :doc:`sila2-codegen new-package <new_package>` for more information about the ``new-package`` command.
