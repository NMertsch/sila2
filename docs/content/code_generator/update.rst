``sila2-codegen update``: Update existing package
=================================================

Use this command to update the auto-generated Python code in an existing package, e.g. after you changed a feature definition, or after an SDK update.

.. runcmd:: sila2-codegen update --help

.. note::

    This will update the existing ``generated/`` directory and add a file ``updated_..._impl.py`` to the ``feature_implementations/`` directory.
    It is intended to help you with migrating your old feature implementation to the updated feature.
