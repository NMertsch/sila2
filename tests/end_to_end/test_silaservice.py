import uuid
from typing import Tuple

import pytest

from sila2.client.client_feature import ClientFeature
from sila2.client.sila_client import SilaClient
from sila2.features.silaservice import SiLAServiceFeature
from sila2.framework.errors.defined_execution_error import DefinedExecutionError
from sila2.framework.errors.validation_error import ValidationError
from sila2.server.sila_server import SilaServer
from tests.utils import generate_port


@pytest.fixture
def server_client() -> Tuple[SilaServer, SilaClient]:
    class TestServer(SilaServer):
        def __init__(self):
            super().__init__(
                server_name="TestServer",
                server_type="TestServer",
                server_uuid=uuid.uuid4(),
                server_version="0.1",
                server_description="A test SiLA2 server",
                server_vendor_url="https://gitlab.com/sila2/sila_python",
            )

    server = TestServer()
    port = generate_port()
    address = "127.0.0.1"

    server.start_insecure(address, port, enable_discovery=False)
    client = SilaClient(address, port, insecure=True)

    return server, client


def test(server_client):
    server, client = server_client

    service = client.SiLAService

    assert isinstance(service, ClientFeature)

    assert service.ServerName.get() == "TestServer" == server.server_name
    assert service.SetServerName("TestServer2") == ()
    assert service.ServerName.get() == "TestServer2" == server.server_name

    assert service.ImplementedFeatures.get() == [SiLAServiceFeature.fully_qualified_identifier]
    assert (
        service.GetFeatureDefinition(SiLAServiceFeature.fully_qualified_identifier).FeatureDefinition
        == SiLAServiceFeature._feature_definition
    )

    with pytest.raises(DefinedExecutionError) as ex:
        _ = service.GetFeatureDefinition("org.silastandard/examples/GreetingProvider/v1")
    assert (
        ex.value.fully_qualified_identifier
        == "org.silastandard/core/SiLAService/v1/DefinedExecutionError/UnimplementedFeature"
    )

    with pytest.raises(ValidationError, match="FullyQualifiedIdentifier"):
        _ = service.GetFeatureDefinition("org.silastandard/core/SiLAService")  # version missing

    assert service.ServerType.get() == "TestServer" == server.server_type
    assert service.ServerUUID.get() == str(server.server_uuid)
    assert service.ServerDescription.get() == "A test SiLA2 server" == server.server_description
    assert service.ServerVersion.get() == "0.1" == server.server_version
    assert service.ServerVendorURL.get() == "https://gitlab.com/sila2/sila_python" == server.server_vendor_url

    with pytest.raises(AttributeError):
        _ = service.Get_ServerType()
    with pytest.raises(AttributeError):
        _ = client.GreetingProvider

    server.stop()


def test_server_init():
    valid_kwargs = {
        "server_name": "TestServer",
        "server_type": "TestServer",
        "server_version": "0.1",
        "server_description": "A test SiLA2 server",
        "server_vendor_url": "https://gitlab.com/sila2/sila_python",
    }

    _ = SilaServer(**valid_kwargs)

    with pytest.raises(ValueError, match="Server name"):
        SilaServer(
            server_name="a" * 256,  # too long
            server_type=valid_kwargs["server_type"],
            server_version=valid_kwargs["server_version"],
            server_description=valid_kwargs["server_description"],
            server_vendor_url=valid_kwargs["server_vendor_url"],
        )

    with pytest.raises(ValueError, match="Server type"):
        SilaServer(
            server_name=valid_kwargs["server_name"],
            server_type="serverType",
            server_version=valid_kwargs["server_version"],
            server_description=valid_kwargs["server_description"],
            server_vendor_url=valid_kwargs["server_vendor_url"],
        )

    with pytest.raises(ValueError, match="Server version"):
        SilaServer(
            server_name=valid_kwargs["server_name"],
            server_type=valid_kwargs["server_type"],
            server_version="1",
            server_description=valid_kwargs["server_description"],
            server_vendor_url=valid_kwargs["server_vendor_url"],
        )

    with pytest.raises(ValueError, match="Server vendor url"):
        SilaServer(
            server_name=valid_kwargs["server_name"],
            server_type=valid_kwargs["server_type"],
            server_version=valid_kwargs["server_version"],
            server_description=valid_kwargs["server_description"],
            server_vendor_url="gitlab.com/sila2",
        )
