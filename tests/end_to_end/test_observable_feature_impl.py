import datetime
import time
from typing import Tuple
from uuid import UUID

import pytest

from sila2.client.client_observable_command_instance import ClientObservableCommandInstance
from sila2.client.sila_client import SilaClient
from sila2.framework.errors.command_execution_not_finished import CommandExecutionNotFinished
from sila2.server.sila_server import SilaServer
from tests.end_to_end.timer_impl import TimerFeature, TimerImpl
from tests.utils import generate_port


@pytest.fixture
def server_client() -> Tuple[SilaServer, SilaClient]:
    class TestServer(SilaServer):
        def __init__(self):
            super().__init__(
                server_name="TestServer",
                server_type="TestServer",
                server_version="0.1",
                server_description="A test SiLA2 server",
                server_vendor_url="https://gitlab.com/sila2/sila_python",
            )

            self.timer = TimerImpl(self)

            self.set_feature_implementation(TimerFeature, self.timer)

    port = generate_port()
    address = "127.0.0.1"
    server = TestServer()
    server.start_insecure(address, port, enable_discovery=False)
    client = SilaClient(address, port, insecure=True)

    return server, client


def test_observable_command(server_client):
    server, client = server_client

    countdown_instance_1 = client.Timer.Countdown(3)
    countdown_instance_2 = client.Timer.Countdown(2)

    assert isinstance(countdown_instance_1, ClientObservableCommandInstance)
    assert isinstance(countdown_instance_2, ClientObservableCommandInstance)

    assert isinstance(countdown_instance_1.execution_uuid, UUID)
    assert countdown_instance_1.execution_uuid != countdown_instance_2.execution_uuid

    with pytest.raises(CommandExecutionNotFinished):
        countdown_instance_1.get_responses()

    assert countdown_instance_1.done is False
    intermediate_responses = countdown_instance_1.subscribe_to_intermediate_responses()

    while not countdown_instance_1.done:
        time.sleep(0.1)
    assert countdown_instance_1.done is True

    assert isinstance(countdown_instance_1.get_responses().Timestamp, datetime.datetime)

    nums = [val.CurrentNumber for val in intermediate_responses]
    assert all(isinstance(num, int) for num in nums)

    server.stop()


def test_observable_property_get(server_client):
    server, client = server_client

    assert isinstance(client.Timer.CurrentTime.get(), datetime.datetime)

    server.stop()


def test_observable_property_subscribe_iter(server_client):
    server, client = server_client

    sub = client.Timer.CurrentTime.subscribe()
    time.sleep(3)
    sub.cancel()

    times = list(sub)
    assert 2 <= len(times) <= 4
    assert all(isinstance(t, datetime.datetime) for t in times)

    server.stop()


def test_observable_property_subscribe_callbacks(server_client):
    server, client = server_client

    sub = client.Timer.CurrentTime.subscribe()
    assert sub.is_cancelled is False
    times = []
    assert len(sub.callbacks) == 0
    sub.add_callback(times.append)
    assert len(sub.callbacks) == 1
    time.sleep(3)
    assert 2 <= len(times) <= 4
    assert all(isinstance(t, datetime.datetime) for t in times)

    sub.clear_callbacks()
    assert len(sub.callbacks) == 0
    sub.cancel()
    assert sub.is_cancelled is True

    with pytest.raises(RuntimeError):
        sub.add_callback(lambda year: None)

    server.stop()
