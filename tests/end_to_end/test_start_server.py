import logging

import pytest

from sila2.client import SilaClient
from sila2.framework import SilaConnectionError
from sila2.server import SilaServer
from sila2.server.encryption import generate_self_signed_certificate
from tests.utils import generate_port

LOCALHOST = "127.0.0.1"


@pytest.fixture
def server() -> SilaServer:
    return SilaServer("TestName", "TestType", "TestDescription", "0.1", "https://gitlab.com/sila2/sila_python")


def test_start_server_with_invalid_parameters(server):
    # chain and key must both be provided, or none of the two
    with pytest.raises(
        ValueError,
        match="For secure connections, either provide both the private key and certificate chain, or none of them",
    ):
        server.start(LOCALHOST, generate_port(), private_key=b"")  # chain missing

    with pytest.raises(
        ValueError,
        match="For secure connections, either provide both the private key and certificate chain, or none of them",
    ):
        server.start(LOCALHOST, generate_port(), cert_chain=b"")  # key missing


def test_start_server_twice(server):
    server.start_insecure(LOCALHOST, generate_port())
    server.stop()

    with pytest.raises(RuntimeError):
        server.start_insecure(LOCALHOST, generate_port())


def test_insecure_server_warns(server, caplog):
    # insecure servers violate the SiLA 2 specs, so a warning should be issued
    caplog.clear()
    with caplog.at_level(logging.WARNING):
        server.start_insecure(LOCALHOST, generate_port())
        assert caplog.records
    server.stop()


def test_self_signed_server_without_discovery(server):
    # self-signed certificate can only be used with discovery enabled
    port = generate_port()
    with pytest.raises(ValueError, match="If discovery is disabled, private key and certificate chain are required"):
        server.start(LOCALHOST, port, enable_discovery=False)


@pytest.mark.filterwarnings("ignore:Connected .* via untrusted certificate:RuntimeWarning")
def test_self_signed_server_with_discovery(server):
    # self-signed certificate should be generated automatically
    port = generate_port()
    try:
        server.start(LOCALHOST, port)

        with pytest.raises(SilaConnectionError):
            _ = SilaClient(LOCALHOST, port, insecure=True)

        with pytest.warns(RuntimeWarning):  # unencrypted communication
            SilaClient.discover(server_uuid=server.server_uuid, timeout=5)
    finally:
        server.stop()


def test_secure_server_without_discovery(server):
    port = generate_port()
    key, cert = generate_self_signed_certificate(server.server_uuid, LOCALHOST)

    server.start(LOCALHOST, port, private_key=key, cert_chain=cert, enable_discovery=False)
    _ = SilaClient(LOCALHOST, port, root_certs=cert)
    server.stop()
