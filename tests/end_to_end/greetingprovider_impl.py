from sila2.framework.feature import Feature
from sila2.server import MetadataDict
from sila2.server.feature_implementation_base import FeatureImplementationBase
from tests.utils import get_feature_definition_str

GreetingProvider = Feature(get_feature_definition_str("GreetingProvider"))


class GreetingProviderImpl(FeatureImplementationBase):
    def SayHello(self, Name: str, *, metadata: MetadataDict) -> str:
        return f"Hello {Name}"

    def get_StartYear(self, *, metadata: MetadataDict) -> int:
        return 2022
