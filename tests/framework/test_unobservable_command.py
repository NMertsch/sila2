import pytest

from sila2.framework import ValidationError
from sila2.framework.command.unobservable_command import UnobservableCommand
from sila2.framework.data_types.boolean import Boolean
from sila2.framework.data_types.constrained import Constrained
from sila2.framework.data_types.string import String
from sila2.framework.feature import Feature
from tests.utils import create_server_stub, get_feature_definition_str


@pytest.fixture
def unobservable_command_feature() -> Feature:
    return Feature(get_feature_definition_str("UnobservableCommand"))


def test_attributes(unobservable_command_feature):
    cmd = unobservable_command_feature._unobservable_commands["TestStringLength"]

    assert isinstance(cmd, UnobservableCommand)
    assert cmd._identifier == "TestStringLength"
    assert cmd._display_name == "Test String Length"
    assert cmd._description == "Takes an integer and a string. Returns True if the integer matches the string length"
    assert cmd.fully_qualified_identifier == "de.unigoettingen/tests/UnobservableCommand/v1/Command/TestStringLength"


def test_parameters(unobservable_command_feature):
    cmd = unobservable_command_feature._unobservable_commands["TestStringLength"]

    assert len(cmd.parameters) == 2
    length_par = cmd.parameters.fields[0]
    assert length_par._identifier == "Length"
    assert isinstance(length_par.data_type, Constrained)

    assert cmd.parameters.fields[1]._identifier == "String"
    assert isinstance(cmd.parameters.fields[1].data_type, String)

    msg = cmd.parameters.to_message(3, "abc")
    params = cmd.parameters.to_native_type(msg)
    assert len(params) == 2

    # fields
    assert params.Length == 3
    assert params.String == "abc"

    # unpacking
    length, string = params
    assert (length, string) == (3, "abc")

    with pytest.raises(TypeError):
        cmd.parameters.to_message(1)
    with pytest.raises(TypeError):
        cmd.parameters.to_message(1, "abc", 3)

    with pytest.raises(ValidationError):
        cmd.parameters.to_message(-1, "")


def test_responses(unobservable_command_feature):
    cmd = unobservable_command_feature._unobservable_commands["TestStringLength"]

    assert len(cmd.responses) == 1
    assert cmd.responses.fields[0]._identifier == "Matches"
    assert isinstance(cmd.responses.fields[0].data_type, Boolean)

    msg = cmd.responses.to_message(True)
    res = cmd.responses.to_native_type(msg)
    assert len(res) == 1

    # fields
    assert res.Matches is True

    # unpacking
    (matches,) = res
    assert matches is True

    with pytest.raises(TypeError):
        cmd.responses.to_message(True, 1)


def test_grpc(unobservable_command_feature):
    cmd = unobservable_command_feature._unobservable_commands["TestStringLength"]

    class UnobservableCommandImpl(unobservable_command_feature._servicer_cls):
        def TestStringLength(self, request, context):
            length, string = cmd.parameters.to_native_type(request)
            return cmd.responses.to_message(len(string) == length)

    server, stub = create_server_stub(unobservable_command_feature._grpc_module, UnobservableCommandImpl())

    # fields
    res = cmd.responses.to_native_type(stub.TestStringLength(cmd.parameters.to_message(3, "abc")))
    assert res.Matches is True

    # unpacking
    (matches,) = cmd.responses.to_native_type(stub.TestStringLength(cmd.parameters.to_message(2, "ghi")))
    assert matches is False


def test_none_packing_and_unpacking(unobservable_command_feature):
    cmd = unobservable_command_feature._unobservable_commands["CommandNoParamNoResponse"]

    param_msg = cmd.parameters.to_message(None)
    assert cmd.parameters.to_native_type(param_msg) == ()

    with pytest.raises(TypeError):
        _ = cmd.parameters.to_message(0)
