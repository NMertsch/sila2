"""Update the example server with the current code generator version"""

import glob
import os
import shutil
from os.path import abspath, dirname, join, relpath
from typing import List, Tuple

from sila2.code_generator.__main__ import new_package

server_root_dir = join(dirname(abspath(__file__)), "example_server")
server_package_name = "sila2_example_server"
out_dir = "new_example_server"

fdl_files = glob.glob(join(server_root_dir, server_package_name, "generated", "*", "*.sila.xml"))
impl_files = glob.glob(join(server_root_dir, server_package_name, "feature_implementations", "*_impl.py"))

new_package(
    feature_definitions=fdl_files,
    package_name=server_package_name,
    output_directory=out_dir,
    overwrite=False,
    generate_main=True,
    git=False,
    lock_controller=False,
    auth_features=False,
)


def copy_server_properties():
    def find_server_information_line_range(filename: str) -> Tuple[List[str], int, int]:
        with open(filename, encoding="utf-8") as server_fp:
            init_start_linenum = -1
            super_init_end_linenum = -1

            server_lines = server_fp.readlines()

            for linenum, _line in enumerate(server_lines):
                if _line.startswith("    def __init__("):
                    init_start_linenum = linenum
                if _line == "        )\n":
                    super_init_end_linenum = linenum

            if init_start_linenum == -1 or super_init_end_linenum == -1:
                raise RuntimeError("Could not find original server information lines")

        return server_lines, init_start_linenum, super_init_end_linenum

    orig_lines, orig_start, orig_end = find_server_information_line_range(
        join(server_root_dir, server_package_name, "server.py")
    )
    new_lines, new_start, new_end = find_server_information_line_range(join(out_dir, server_package_name, "server.py"))

    with open(join(out_dir, server_package_name, "server.py"), "w", encoding="utf-8") as combined_server_fp:
        for line in new_lines[:new_start]:
            combined_server_fp.write(line)
        for line in orig_lines[orig_start:orig_end]:
            combined_server_fp.write(line)
        for line in new_lines[new_end:]:
            combined_server_fp.write(line)


# copy server properties in server.py
copy_server_properties()

# copy impl files
for file in impl_files:
    with open(file, encoding="utf-8") as fp:
        content = fp.read()
    with open(join(out_dir, relpath(file, server_root_dir)), "w", encoding="utf-8") as fp:
        fp.write(content)

# copy readme
with open(join(server_root_dir, "README.md"), encoding="utf-8") as orig_fp, open(
    join(out_dir, "README.md"), "w", encoding="utf-8"
) as new_fp:
    new_fp.write(orig_fp.read())

# copy generated package to example_server/
shutil.rmtree(server_root_dir)
os.rename(out_dir, server_root_dir)
